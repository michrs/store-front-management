from functions import customer_operations
from functions import line_operations
from functions import databases

def main():
    databases.create_database()

    while True:
        print('\nQueue System Menu:')
        print('1. Add Customer to Queue')
        print('2. Remove Customer from Queue')
        print('3. Move Customer in Queue')
        print('4. Create New Line')
        print('5. Edit Line')
        print('6. Delete Line')
        print('7. Display List of People in a Line')
        print('8. Exit')

        choice = input('Enter your choice (1-8): ')

        if choice == '1':
            customer_operations.add_customer()
        elif choice == '2':
            customer_operations.remove_customer()
        elif choice == '3':
            customer_operations.move_customer()
        elif choice == '4':
            line_operations.create_line()
        elif choice == '5':
            line_operations.edit_line()
        elif choice == '6':
            line_operations.delete_line()
        elif choice == '7':
            line_operations.display_line()
        elif choice == '8':
            print('Exiting the Queue System. Goodbye!')
            break
        else:
            print('Invalid choice. Please enter a number between 1 and 8.')

if __name__ == "__main__":
    main()
