import sqlite3

def create_database():
    conn = sqlite3.connect('queue_system.db')
    cursor = conn.cursor()

    # Create the Line table
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS Lines (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            avg_wait_time INTEGER NOT NULL
        )
    ''')

    # Fetch existing line names from the Lines table
    cursor.execute('SELECT name FROM Lines')
    existing_lines = [row[0] for row in cursor.fetchall()]

    # Create a table for each line if it doesn't exist
    for line_name in existing_lines:
        create_line_table(cursor, line_name)

    conn.commit()
    conn.close()

def create_line_table(cursor, line_name):
    # Replace spaces in the line name with underscores to create a valid table name
    table_name = line_name.replace(' ', '_')

    cursor.execute(f'''
        CREATE TABLE IF NOT EXISTS {table_name} (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            phone TEXT NOT NULL,
            position INTEGER NOT NULL
        )
    ''')