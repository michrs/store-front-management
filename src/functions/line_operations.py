import sqlite3
from .databases import create_line_table

def create_line():
    line_name = input('Enter the line name: ')
    avg_wait_time = int(input('Enter the average wait time for a customer (in minutes): '))

    conn = sqlite3.connect('queue_system.db')
    cursor = conn.cursor()

    cursor.execute('INSERT INTO Lines (name, avg_wait_time) VALUES (?, ?)', (line_name, avg_wait_time))
    conn.commit()

    print(f'Line "{line_name}" created with an average wait time of {avg_wait_time} minutes.')

    # Call the function to create the corresponding line table
    create_line_table(cursor, line_name)

    conn.close()

def delete_line():
    lines = get_lines()
    if not lines:
        print('Error: No lines available. Please create a line first.')
        return

    print('Available lines:')
    for idx, line in enumerate(lines, start=1):
        print(f'{idx}. {line}')

    while True:
        try:
            line_idx = int(input('Enter the number corresponding to the line to delete: '))
            if 1 <= line_idx <= len(lines):
                break
            else:
                print('Invalid line number. Please enter a valid line number.')
        except ValueError:
            print('Invalid input. Please enter a number.')

    line_name_to_delete = lines[line_idx - 1]

    conn = sqlite3.connect('queue_system.db')
    cursor = conn.cursor()

    cursor.execute('DELETE FROM Lines WHERE name = ?', (line_name_to_delete,))
    conn.commit()

    if cursor.rowcount > 0:
        print(f'Line "{line_name_to_delete}" deleted.')
    else:
        print(f'Error: Line "{line_name_to_delete}" does not exist.')

    conn.close()

def edit_line():
    lines = get_lines()
    if not lines:
        print('Error: No lines available. Please create a line first.')
        return

    print('Available lines:')
    for idx, line in enumerate(lines, start=1):
        print(f'{idx}. {line}')

    while True:
        try:
            line_idx = int(input('Enter the number corresponding to the line: '))
            if 1 <= line_idx <= len(lines):
                break
            else:
                print('Invalid line number. Please enter a valid line number.')
        except ValueError:
            print('Invalid input. Please enter a number.')

    line_name_to_edit = lines[line_idx - 1]

    conn = sqlite3.connect('queue_system.db')
    cursor = conn.cursor()

    # Choose what to edit
    print('Choose what to edit:')
    print('1. Edit Line Name')
    print('2. Edit Average Wait Time')
    choice = input('Enter your choice (1 or 2): ')

    if choice == '1':
        new_name = input(f'Enter the new name for the line "{line_name_to_edit}": ')
        cursor.execute('UPDATE Lines SET name = ? WHERE name = ?', (new_name, line_name_to_edit))
        print(f'Line name updated for "{line_name_to_edit}".')
    elif choice == '2':
        new_avg_wait_time = int(input(f'Enter the new average wait time for "{line_name_to_edit}" (in minutes): '))
        cursor.execute('UPDATE Lines SET avg_wait_time = ? WHERE name = ?', (new_avg_wait_time, line_name_to_edit))
        print(f'Average wait time updated for "{line_name_to_edit}" to {new_avg_wait_time} minutes.')
    else:
        print('Invalid choice. Please enter either 1 or 2.')

    conn.commit()
    conn.close()

def display_line():
    lines = get_lines()
    if not lines:
        print('Error: No lines available. Please create a line first.')
        return

    print('Available lines:')
    for idx, line in enumerate(lines, start=1):
        print(f'{idx}. {line}')

    while True:
        try:
            line_idx = int(input('Enter the number corresponding to the line: '))
            if 1 <= line_idx <= len(lines):
                break
            else:
                print('Invalid line number. Please enter a valid line number.')
        except ValueError:
            print('Invalid input. Please enter a number.')

    line_name = lines[line_idx - 1]

    conn = sqlite3.connect('queue_system.db')
    cursor = conn.cursor()

    # Replace spaces with underscores in the line name when checking for the existence of the table
    line_table = line_name.replace(' ', '_')
    cursor.execute(f'SELECT name, phone, position FROM {line_table} ORDER BY position')
    customers = cursor.fetchall()

    if customers:
        print(f'People in the {line_name} queue:')
        for customer in customers:
            print(f'Position: {customer[2]}, Name: {customer[0]}, Phone: {customer[1]}')
    else:
        print(f'No people found in the {line_name} queue.')

    conn.close()

def get_lines():
    conn = sqlite3.connect('queue_system.db')
    cursor = conn.cursor()

    cursor.execute('SELECT name FROM Lines')
    lines = [line[0] for line in cursor.fetchall()]

    conn.close()
    return lines
