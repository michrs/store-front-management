import sqlite3
import re
from .line_operations import get_lines

def validate_name(name):
    # Correct the capitalization of the name (first letter capital, rest lower case)
    corrected_name = name.title()

    # Check if the corrected name is within the character limit
    if not re.match("^[a-zA-Z\s]{1,50}$", corrected_name):
        print('Error: Invalid name. Please use alphabets and spaces only, up to 50 characters.')
        return None
    return corrected_name

def validate_phone(phone):
    # Remove non-numeric characters (excluding digits 0-9 and spaces)
    phone = re.sub(r'[^0-9\s]', '', phone)

    # Check if the phone number has a minimum of 10 characters and a maximum of 18 characters
    if not re.match("^[0-9\s]{10,18}$", phone):
        print('Error: Invalid phone number. Please use numbers, with a minimum of 10 characters and a maximum of 18 characters.')
        return None
    return phone

def add_customer():
    while True:
        name = input('Enter customer name: ')
        validated_name = validate_name(name)
        if validated_name is not None:
            name = validated_name
            break
        else:
            print('Please enter a valid name.')

    while True:
        phone = input('Enter customer phone: ')
        validated_phone = validate_phone(phone)
        if validated_phone is not None:
            phone = validated_phone
            break
        else:
            print('Please enter a valid phone number.')

    lines = get_lines()
    if not lines:
        print('Error: No lines available. Please create a line first.')
        return

    print('Available lines:')
    for idx, line in enumerate(lines, start=1):
        print(f'{idx}. {line}')

    line_idx = int(input('Enter the number corresponding to the line: '))
    if line_idx < 1 or line_idx > len(lines):
        print('Error: Invalid line number.')
        return

    line = lines[line_idx - 1]

    conn = sqlite3.connect('queue_system.db')
    cursor = conn.cursor()
    line_table = line.replace(' ', '_')

    cursor.execute(f'''
        INSERT INTO {line_table} (name, phone, position)
        VALUES (?, ?, (SELECT COALESCE(MAX(position), 0) + 1 FROM {line_table}))
    ''', (name, phone))
    conn.commit()

    print(f'{name} added to the {line} queue.')

    conn.close()

def remove_customer():
    lines = get_lines()
    if not lines:
        print('Error: No lines available. Please create a line first.')
        return

    print('Available lines:')
    for idx, line in enumerate(lines, start=1):
        print(f'{idx}. {line}')

    while True:
        try:
            line_idx = int(input('Enter the number corresponding to the line: '))
            if 1 <= line_idx <= len(lines):
                break
            else:
                print('Invalid line number. Please enter a valid line number.')
        except ValueError:
            print('Invalid input. Please enter a number.')

    line = lines[line_idx - 1]
    conn = sqlite3.connect('queue_system.db')
    cursor = conn.cursor()
    line_table = line.replace(' ', '_')

    cursor.execute(f'SELECT rowid, name, phone, position FROM {line_table} ORDER BY position')
    customers = cursor.fetchall()

    if not customers:
        print(f'No customers found in the {line} queue.')
        conn.close()
        return

    print(f'Customers in the {line} queue:')
    for idx, customer in enumerate(customers, start=1):
        print(f'{idx}. {customer[1]} - {customer[0]}')  # Displaying position and name

    while True:
        try:
            position = int(input('Enter the position of the customer to remove (0 to cancel): '))
            if position == 0:
                print('Removal canceled.')
                return
            elif 1 <= position <= len(customers):
                break
            else:
                print('Invalid position. Please enter a valid position or 0 to cancel.')
        except ValueError:
            print('Invalid input. Please enter a number or 0 to cancel.')

    # Confirm removal
    confirm_removal = input(f'Confirm removal of customer at position {position}? (yes/no): ').lower()
    if confirm_removal != 'yes':
        print('Removal canceled.')
        return

    # Retrieve customer information before deletion
    customer_to_remove = customers[position - 1]
    customer_name = customer_to_remove[1]
    customer_phone = customer_to_remove[2]

    # Update the positions before removal
    cursor.execute(f'UPDATE {line_table} SET position = position - 1 WHERE position > ?', (position,))
    
    # Remove the customer from the specified position
    cursor.execute(f'DELETE FROM {line_table} WHERE rowid = ?', (customer_to_remove[0],))
    conn.commit()

    print(f'Customer {customer_name} with phone number {customer_phone} removed from the {line} queue.')

    conn.close()

def move_customer():
    lines = get_lines()
    if not lines:
        print('Error: No lines available. Please create a line first.')
        return

    print('Available lines:')
    for idx, line in enumerate(lines, start=1):
        print(f'{idx}. {line}')

    while True:
        try:
            line_idx = int(input('Enter the number corresponding to the line: '))
            if 1 <= line_idx <= len(lines):
                break
            else:
                print('Invalid line number. Please enter a valid line number.')
        except ValueError:
            print('Invalid input. Please enter a number.')

    line = lines[line_idx - 1]

    conn = sqlite3.connect('queue_system.db')
    cursor = conn.cursor()
    line_table = line.replace(' ', '_')

    cursor.execute(f'SELECT rowid, name, phone, position FROM {line_table} ORDER BY position')
    customers = cursor.fetchall()

    if not customers:
        print(f'No customers found in the {line} queue.')
        conn.close()
        return

    print(f'Customers in the {line} queue:')
    for idx, customer in enumerate(customers, start=1):
        print(f'{idx}. {customer[1]} - {customer[0]}')  # Displaying position and name

    while True:
        try:
            from_position = int(input('Enter the current position of the customer to move (0 to cancel): '))
            if from_position == 0:
                print('Move canceled.')
                return
            elif 1 <= from_position <= len(customers):
                break
            else:
                print('Invalid position. Please enter a valid position or 0 to cancel.')
        except ValueError:
            print('Invalid input. Please enter a number or 0 to cancel.')

    while True:
        try:
            to_position = int(input('Enter the new position for the customer (0 to cancel): '))
            if to_position == 0:
                print('Move canceled.')
                return
            elif 1 <= to_position <= len(customers):
                break
            else:
                print('Invalid position. Please enter a valid position or 0 to cancel.')
        except ValueError:
            print('Invalid input. Please enter a number or 0 to cancel.')

    if from_position == to_position:
        print('Move canceled. The source and destination positions are the same.')
        return

    cursor.execute(f'''
        UPDATE {line_table}
        SET position = ? - position + ?
        WHERE position IN (?, ?)
    ''', (to_position, from_position, from_position, to_position))
    conn.commit()

    print(f'Customer at position {from_position} moved to position {to_position} in the {line} queue.')

    conn.close()

def update_customer_positions(cursor, line_table):
    conn = sqlite3.connect('queue_system.db')
    cursor = conn.cursor()

    cursor.execute('SELECT rowid FROM {} ORDER BY position'.format(line_table))
    customer_rowids = [row[0] for row in cursor.fetchall()]

    for idx, customer_rowid in enumerate(customer_rowids, start=1):
        cursor.execute('UPDATE {} SET position = ? WHERE rowid = ?'.format(line_table), (idx, customer_rowid))

    conn.commit()
    conn.close()
